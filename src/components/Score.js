import React, { Component } from 'react';
import Time from './Time';
import '../static/Score.css'

export default class Score extends Component {
    constructor(props){
        super(props);

        this.timeRef = React.createRef();
    }

    render(){
        console.log("TIME REF: ", this.timeRef);
        const { score } = this.props;
        return(
            <div className="row score">
                <div className="score-left col-4">
                    <p>{score}</p>
                </div>
                <div className="score-mid col-4">
                    <Time ref={this.timeRef} countHiddenItems={this.props.countHiddenItems} />
                </div>
                <div className="score-right row col-4">
                    <div className="buttongame button-start col-6">
                        <button type="button" onClick={() => this.timeRef.current.startGame()}>Start</button>
                    </div>
                    <div className="buttongame button-end col-6">
                        <button type="button">Give up</button>
                    </div>
                </div>
            </div>
        )
    }
}