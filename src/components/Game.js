import React, { Component } from 'react';
import Board from './Board';

export default class Game extends Component {

    render(){
        return(
            <div className="game">
                <h1 style={{ marginBottom: "0", marginTop: "20px"}}>Heros game</h1>
                <Board />
            </div>
        )
    }
}