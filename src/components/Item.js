import React, { Component } from 'react';
import anime from 'animejs';
import '../static/Item.css';

export default class Item extends Component {
    constructor(props){
        super(props);
        this.itemRef = React.createRef();
    }

    componentDidMount() {
        this.itemRef.current = anime({
            targets: ".item-button",
            // translateX: -50,
            // translateY: -50,
            opacity: 1,
            delay: function(el, i) {
                return i * 50;
            },
            duration: 900,
            loop: false,
            direction: "alternate",
            easing: "easeOutExpo"
        })
    }

    render(){
        const { changeSelected, item, onClickToShake, shake } = this.props;
        return(
            <button className={item.hidden? `item-button hidden`:`item-button ${shake}`} ref={this.itemRef} style={{padding: "10px"}} onClick={changeSelected} >
                <span className={item.selected? "background-item selected":"background-item"} onClick={onClickToShake}></span>
                <img src={`/images/img${item.id}.svg`} alt="item img" width="60px" />
            </button>
        )
    }
}