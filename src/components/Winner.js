import React, { Component } from 'react';
import '../static/Board.css'

export default class Winner extends Component {

    render(){
        return(
            <div className="winner">
                <span></span>
                <img src={`/images/winner.svg`} width="50%" alt="win" />
                <p>{this.props.finalScore}</p>
                <h1>You win</h1>
                <button className="again-button" type="button">Again</button>
            </div>
        )
    }
}