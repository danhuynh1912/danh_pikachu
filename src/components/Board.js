import React, { Component } from 'react';
import Item from './Item';
import Score from './Score';
import Winner from './Winner';
import '../static/Board.css'

export default class Board extends Component {
    constructor(props){
        super(props);
        this.state = {
            Rows: [],
            xFirst: 0,
            yFirst: 0,
            shake: ''
        }

        this.allSelectedToFalse = this.allSelectedToFalse.bind(this);
        this.itemRef = React.createRef();
    }

    componentDidMount(){
        var imgsId = [];

        for(let i=0; i < 72; i++) {
            if(i % 2 === 0){
                imgsId.push(Math.floor(Math.random() * 32))
            }
            else {
                imgsId.push(imgsId[i-1]);
            }
        }

        const shuffledArray = imgsId.sort((a, b) => 0.5 - Math.random());

        var imgsRow0 = [{id: -1, hidden: true}];
        var imgsRow1 = [{id: -1, hidden: true}];
        var imgsRow2 = [{id: -1, hidden: true}];
        var imgsRow3 = [{id: -1, hidden: true}];
        var imgsRow4 = [{id: -1, hidden: true}];
        var imgsRow5 = [{id: -1, hidden: true}];
        var imgsRow6 = [{id: -1, hidden: true}];
        var imgsRow7 = [{id: -1, hidden: true}];

        for(let i=0; i<shuffledArray.length; i++) {
            if(i>=0 && i<12) {
                imgsRow0.push( {id: -1, hidden: true} );
                imgsRow7.push( {id: -1, hidden: true} );
                imgsRow1.push( {id: shuffledArray[i], selected: false, hidden: false, shaking:  ''} );
            }
            if(i>=12 && i<24) {
                imgsRow2.push( {id: shuffledArray[i], selected: false, hidden: false, shaking:  ''} );
            }
            if(i>=24 && i<36) {
                imgsRow3.push( {id: shuffledArray[i], selected: false, hidden: false, shaking:  ''} );
            }
            if(i>=36 && i<48) {
                imgsRow4.push( {id: shuffledArray[i], selected: false, hidden: false, shaking:  ''} );
            }
            if(i>=48 && i<60) {
                imgsRow5.push( {id: shuffledArray[i], selected: false, hidden: false, shaking:  ''} );
            }
            if(i>=60 && i<72) {
                imgsRow6.push( {id: shuffledArray[i], selected: false, hidden: false, shaking:  ''} );
            }
        }

        imgsRow0.push( {id: -1, hidden: true} );
        imgsRow1.push( {id: -1, hidden: true} );
        imgsRow2.push( {id: -1, hidden: true} );
        imgsRow3.push( {id: -1, hidden: true} );
        imgsRow4.push( {id: -1, hidden: true} );
        imgsRow5.push( {id: -1, hidden: true} );
        imgsRow6.push( {id: -1, hidden: true} );
        imgsRow7.push( {id: -1, hidden: true} );

        this.setState({
            Rows: [imgsRow0, imgsRow1, imgsRow2, imgsRow3, imgsRow4, imgsRow5, imgsRow6, imgsRow7]
        })
    }

    countSelected = () => {  //count the number of items which are selected
        const { Rows } = this.state;
        var countSelected = 0;
        Rows.forEach((items, x) => {
            items.forEach((item, y) => {
                if(item.selected) {
                    countSelected ++;
                }
            })
        }) 
        return countSelected;
    }

    allSelectedToFalse() {  //make all the items selected to false 
        const { Rows } = this.state;
        Rows.forEach((items, a) => {
            items.forEach((item, b) => {
                if(item.selected) {
                    item.selected = false;
                    item.shaking = '';
                    this.setState({
                        Rows: Rows
                    })
                }
            })
        }) 
    }

    selectItem = (x, y, shake) => {  //select the item
        const { Rows } = this.state;
        Rows[x][y].selected = !Rows[x][y].selected;
        this.setState({
            // Rows: [
            //     ...Rows.slice(0, x),
            //     [
            //         ...Rows[x].slice(0, y),
            //         { ...Rows[x][y], selected: !Rows[x][y].selected, shaking: shake },
            //         ...Rows[x].slice(y + 1)
            //     ],
            //     ...Rows.slice(x + 1)
            // ]
            Rows: Rows
        })
    }

    hiddenItem = (x, y) => {   //hidden the item 
        const { Rows } = this.state;
        Rows[x][y].hidden = true;
        this.setState({
            Rows: Rows
        })
    }

    shakeItem = (x, y) => {   //hidden the item 
        const { Rows } = this.state;
        Rows[x][y].shaking = 'shake';
        this.setState({
            Rows: Rows
        })
    }

    setStateXY = (x, y) => {
        this.setState({
            xFirst: x,
            yFirst: y
        })
    }

    //functions to check line
    checkLineX = (y1, y2, x) => {
        const { Rows } = this.state;
        var yMax, yMin;
        yMax = Math.max(y1, y2);
        yMin = Math.min(y1, y2);
        for ( var i = yMin + 1; i < yMax; i++ ) {
            if( !Rows[x][i].hidden ) return false
        }
        return true
    }

    checkLineY = (x1, x2, y) => {
        const { Rows } = this.state;
        var xMax, xMin;
        xMax = Math.max(x1, x2);
        xMin = Math.min(x1, x2);
        for ( var i = xMin + 1; i < xMax; i++ ) {
            if( !Rows[i][y].hidden ) return false
        }
        return true
    }

    checkRectX = (x1, y1, x2, y2) => {
        var yMax, yMin, xMax, xMin;
        yMax = Math.max(y1, y2);
        yMin = Math.min(y1, y2);

        xMax = yMax === y1? x1:x2;
        xMin = yMin === y1? x1:x2;

        if(x1 !== x2) {
            for(var i = yMin+1; i < yMax; i++) {
                if(this.checkLineX(yMin, i + 1, xMin) 
                && this.checkLineY(xMin, xMax, i) 
                && this.checkLineX(i - 1, yMax, xMax)) {
                    return i        
                }
            }
            return -1    
        }
        else return -1
    }

    checkRectY = (x1, y1, x2, y2) => {
        var yMax, yMin, xMax, xMin;
        
        xMax = Math.max(x1, x2);
        xMin = Math.min(x1, x2);
        
        yMax = xMax === x1? y1:y2;
        yMin = xMin === x1? y1:y2;

        if(y1 !== y2) {
            for(var i = xMin+1; i < xMax; i++) {
                if(this.checkLineY(xMin, i + 1, yMin) 
                && this.checkLineX(yMin, yMax, i) 
                && this.checkLineY(i - 1, xMax, yMax)) {
                    return i        
                }
            }
            return -1
        }
        else return -1
    }

    checkMoreLineX = (x1, y1, x2, y2, type) => {
        const { Rows } = this.state;
        var yMax, yMin, xMax, xMin;
        yMax = Math.max(y1, y2);
        yMin = Math.min(y1, y2);

        if(yMax === yMin) {
            xMax = Math.max(x1, x2);
            xMin = Math.min(x1, x2);
        }
        else {
            xMax = yMax === y1? x1:x2;
            xMin = yMin === y1? x1:x2;
        }

        var y = yMax, row = xMin;
        if(type === -1) {
            y = yMin; 
            row = xMax;
        }

        if(this.checkLineX(yMin, yMax, row)) {
            for(var i = y; i <= 13 && i>=0; i += type) {
                if((Rows[row][i].hidden && i === y) 
                || (Rows[xMin][i].hidden && Rows[xMax][i].hidden) 
                || (yMin === yMax && i === y)) {
                    if(this.checkLineY(xMin, xMax, i)) return i;
                }
                else {
                    break;
                }
            }
            return -1
        }
        return -1
    }

    checkMoreLineY = (x1, y1, x2, y2, type) => {
        const { Rows } = this.state;
        var yMax, yMin, xMax, xMin;
        xMax = Math.max(x1, x2);
        xMin = Math.min(x1, x2);

        if(xMax === xMin) {
            yMax = Math.max(y1, y2);
            yMin = Math.min(y1, y2);
        }
        else {
            yMax = xMax === x1? y1:y2;
            yMin = xMin === x1? y1:y2;
        }

        var x = xMax, col = yMin;
        if(type === -1) {
            x = xMin; 
            col = yMax;
        }

        if(this.checkLineY(xMin, xMax, col)) {
            for(var i = x; i <= 7 && i>=0; i += type) {
                if((Rows[i][col].hidden && i === x)                                         
                || (Rows[i][yMin].hidden && Rows[i][yMax].hidden) 
                || (xMin === xMax && i === x)) {
                    if(this.checkLineX(yMin, yMax, i)) return i;
                }
                else {
                    break;
                }
            }
            return -1
        }
        return -1
    }

    changeSelected = (x, y) => {
        const { Rows, xFirst, yFirst } = this.state;
        
        if(Rows[x][y].hidden === true) {
            return 
        }
        else {
            if(this.countSelected() >= 2) {
                this.allSelectedToFalse();
                this.selectItem(x, y);
                this.setStateXY(x, y);
            }
            else {
                if(this.countSelected() === 1) {
                    if( Rows[xFirst][yFirst].id === Rows[x][y].id ){
                        if(x === xFirst && y === yFirst) {
                            this.selectItem(x, y)
                        }
                        else {
                            if((x === xFirst && this.checkLineX(y, yFirst, x)) 
                            || (y === yFirst && this.checkLineY(x, xFirst, y))
                            || (this.checkRectX(x, y, xFirst, yFirst) !== -1)
                            || (this.checkRectY(x, y, xFirst, yFirst) !== -1)
                            || (this.checkMoreLineX(x, y, xFirst, yFirst, 1) !== -1)
                            || (this.checkMoreLineX(x, y, xFirst, yFirst, -1) !== -1)
                            || (this.checkMoreLineY(x, y, xFirst, yFirst, 1) !== -1)
                            || (this.checkMoreLineY(x, y, xFirst, yFirst, -1) !== -1)) {
                                this.hiddenItem(xFirst, yFirst);
                                this.hiddenItem(x, y);
                                this.allSelectedToFalse();
                            }
                            else{
                                this.selectItem(x, y, 'shake');
                                this.shakeItem(x, y);
                                this.shakeItem(xFirst, yFirst);
                            }
                        }
                    }
                    else {
                        this.selectItem(x, y, 'shake');
                        this.shakeItem(x, y);
                        this.shakeItem(xFirst, yFirst);
                    }
                }
                else {
                    if(this.countSelected() === 0) {
                        this.selectItem(x, y);
                        this.setStateXY(x, y);
                    }
                }
            }
        }
        
    }

    // onClickToShake = () => {
    //     const { shake } = this.state;
    //     this.setState({
    //         shake: ''
    //     })
    //     this.setState({
    //         shake: ''
    //     })
    // }

    render(){
        const { Rows, shake } = this.state;
        var countNotHiddenItems = 0, score = 0;
        var countHiddenItems = 0;
        Rows.forEach(items => {
            items.forEach(item => {
                if(!item.hidden) countNotHiddenItems++;
                else countHiddenItems++;
            })
        }) 

        score = (72 - countNotHiddenItems) * 100
        console.log("ITEM REF: ", this.itemRef)
        return(
            <div className="board">
                { !countNotHiddenItems > 0 && <Winner finalScore={score}/> }
                { Rows.length > 0 && countNotHiddenItems > 0 &&
                    Rows.map((item, x) => (

                        <div key={x} className="board-row" style={{height: x===0 || x===7? "40px":"auto"}} >
                            { item.length > 0 &&
                                item.map((item, y) => (
                                    <Item key={y} shake={shake} item={item} changeSelected={() => this.changeSelected(x, y)} ref={this.itemRef} shake={item.shaking} />
                                )) }
                        </div>

                    )) 
                }
                { countNotHiddenItems > 0 && <Score score={score} countHiddenItems={countHiddenItems} />}
            </div>
        )
    }
}